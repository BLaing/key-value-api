# Versioned Key-Value Pair API

This API exposes endpoints to manage key-value pairs, each key with a history of each stored version.

Use cases:

* This API could be used for centralizing configuration values for a server, or a group of them.

## Running the project

Pre-requisites: Node, NPM.

Installing project dependencies: `npm install`

Run a live development server: `npm run serve`

## Running tests

With the project dependencies installed, run: `npm run test`.
