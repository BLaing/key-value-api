import { Server } from 'http';
import app from './app';

export default async (port: number): Promise<Server> => {
  return app.listen(port);
};
