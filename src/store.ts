export interface KeyData {
  versions: { [key: number]: unknown };
  latestVersion: number;
  latestValue: unknown;
}

class Store {
  constructor(private dataSource: string) {
    try {
      Store.data = require(dataSource);
    } catch (e) {
      console.log('Error initializing store, using default: ', e);
    }
  }

  private static data: { [key: string]: KeyData } = {
    foo: {
      versions: {
        1: 'bar',
      },
      latestVersion: 1,
      latestValue: 'bar',
    },
  };

  public getDatasource(): string {
    return this.dataSource;
  }

  public get(key: string, version: number | 'latest' = 'latest'): { version: number; value: unknown } {
    const keyData = Store.data[key];
    if (keyData === undefined) {
      throw { status: 404, message: `Key ${key} was not found in Store.` };
    }

    if (version === 'latest') {
      return {
        version: keyData.latestVersion,
        value: keyData.latestValue,
      };
    }

    const value = keyData.versions[version];
    if (value === undefined) {
      throw { status: 404, message: `Version ${version} for key ${key} was not found in Store.` };
    }
    return {
      version,
      value,
    };
  }

  public addVersion<T>(key: string, value: T): { key: string; version: number; value: T } {
    const nextVersionNumber = (Store.data[key]?.latestVersion ?? 0) + 1;
    const versions = Store.data[key]?.versions ?? {};
    versions[nextVersionNumber] = value;

    Store.data[key] = {
      versions,
      latestValue: value,
      latestVersion: nextVersionNumber,
    };

    return {
      key,
      value,
      version: nextVersionNumber,
    };
  }
}

let store: Store;

export const getStore = (dataSource = './data.json'): Store => {
  if (!store || store.getDatasource() !== dataSource) {
    store = new Store(dataSource);
  }
  return new Store(dataSource);
};
