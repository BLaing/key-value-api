import { Request, Response } from 'express';
import { getStore } from './store';

interface GetValueParams {
  key: string;
  version?: number;
}

interface AddVersionParams {
  key: string;
  value: unknown;
}

export const getValue = async (req: Request, res: Response): Promise<Response> => {
  if (req.method !== 'GET' && req.method !== 'OPTIONS') {
    return res.status(405).send();
  }

  const data: GetValueParams = req.body;
  if (!data.key) {
    return res.status(400).send({ message: "Missing 'key' parameter in request." });
  }

  const store = getStore();

  try {
    return res.send({ value: store.get(data.key, data.version).value });
  } catch (e) {
    return res.status(e.status).send({ message: e.message });
  }
};

export const addVersion = async (req: Request, res: Response): Promise<Response> => {
  if (req.method !== 'PUT' && req.method !== 'OPTIONS') {
    return res.status(405).send();
  }

  const data: AddVersionParams = req.body;
  if (data.key === undefined || data.value === undefined) {
    return res.status(400).send({ message: "'key' and 'value' must be provided" });
  }

  const store = getStore();

  return res.send(store.addVersion(data.key, data.value));
};
