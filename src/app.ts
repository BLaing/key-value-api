import express from 'express';
import { addVersion, getValue } from './controllers';
const app = express();
app.use(express.json());

app.get('/', getValue);
app.put('/', addVersion);

export default app;
