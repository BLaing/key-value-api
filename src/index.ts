import startServer from "./server";

const PORT = +(process.env.PORT || 4000);

async function main() {
  await startServer(PORT);
}

main()
  .then(() => console.log(`API listening at: http://localhost:${PORT}`))
  .catch((error) => {
    console.error("An error has ocurred trying to start the server: ", error);
    process.exit();
  });
