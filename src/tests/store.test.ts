import { getStore } from '../store';

import testData from '../data.json';

describe('Get key-values from Store', () => {
  it('Should raise an error error with unexisting key.', () => {
    expect.assertions(2);
    const store = getStore('./data.json');
    try {
      store.get('non-existing');
    } catch (error) {
      expect(error.status).toBe(404);
      expect(error.message).toBe('Key non-existing was not found in Store.');
    }
  });

  it('Should raise an error with unexisting version.', () => {
    expect.assertions(2);
    const key = Object.keys(testData).length > 0 ? 'foo' : Object.keys(testData)[0];
    const version = -1;
    const store = getStore('./data.json');
    try {
      store.get(key, version);
    } catch (error) {
      expect(error.status).toBe(404);
      expect(error.message).toBe(`Version ${version} for key ${key} was not found in Store.`);
    }
  });

  it('Should return the correct value for an existing key and latest version.', () => {
    expect.assertions(2);
    const key = 'foo';
    const value = testData.foo.latestValue;
    const version = testData.foo.latestVersion;
    const store = getStore('./data.json');
    try {
      const data = store.get(key);
      expect(data.value).toBe(value);
      expect(data.version).toBe(version);
    } catch (e) {
      console.error(e);
    }
  });

  it('Should return the correct value for an existing key and specific version.', () => {
    expect.assertions(2);
    const key = 'name';
    const version = 1;
    const value = testData.name.versions[version];
    const store = getStore('./data.json');
    try {
      const data = store.get(key, version);
      expect(data.value).toBe(value);
      expect(data.version).toBe(version);
    } catch (e) {
      console.error(e);
    }
  });
});

describe('Add versions to the store', () => {
  it('Should successfully add a new version to an existing key.', () => {
    expect.assertions(5);
    const key = 'foo';
    const value = 'changed value';
    const { latestVersion } = testData[key];

    const store = getStore('./data.json');
    const data = store.addVersion(key, value);

    expect(data.key).toBe(key);
    expect(data.value).toBe(value);
    expect(data.version).toBe(latestVersion +1);

    const getData = store.get(key);
    expect(getData.value).toBe(value);
    expect(getData.version).toBe(latestVersion +1);
  });

  it('Should successfully add a new version to an unexisting key.', () => {
    expect.assertions(5);
    const key = 'new';
    const value = 'new value';

    const store = getStore('./data.json');
    const data = store.addVersion(key, value);

    expect(data.key).toBe(key);
    expect(data.value).toBe(value);
    expect(data.version).toBe(1);

    const getData = store.get(key);
    expect(getData.value).toBe(value);
    expect(getData.version).toBe(1);
  })
});
