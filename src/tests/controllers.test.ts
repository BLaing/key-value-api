import supertest from 'supertest';
import app from '../app';

describe('GET: /', () => {
  it('Should return a 400 to a request with no "key" parameter.', async () => {
    const response = await supertest(app).get('/');
    return expect(response.status).toBe(400);
  });

  it('Should return a 404 to a request with a non-existing key.', async () => {
    const response = await supertest(app).get('/').send({ key: 'non-existing' });
    return expect(response.status).toBe(404);
  });

  it('Should return a 404 to a request with a non-existing version for an existing key.', async () => {
    const response = await supertest(app).get('/').send({ key: 'foo', version: 99 });
    return expect(response.status).toBe(404);
  });

  it('Should return a 200 to a request with an existing version for an existing key.', async () => {
    const response = await supertest(app).get('/').send({ key: 'foo', version: 1 });
    expect(response.body.value).toBe('bar');
    return expect(response.status).toBe(200);
  });

  it('Should return a 200 to a request with an existing key and no version.', async () => {
    const response = await supertest(app).get('/').send({ key: 'name' });
    expect(response.body.value).toBe('Laing');
    return expect(response.status).toBe(200);
  });
});

describe('PUT: /', () => {
  it('Should return a 400 to a request with no "key" parameter.', async () => {
    const response = await supertest(app).put('/').send({ value: 1 });
    return expect(response.status).toBe(400);
  });

  it('Should return a 400 to a request with no "value" parameter.', async () => {
    const response = await supertest(app).put('/').send({ key: 'foo' });
    return expect(response.status).toBe(400);
  });

  it('Should return a 200 to a request with existing "key" and "value" parameters.', async () => {
    const response = await supertest(app).put('/').send({ key: 'foo', value: 'new' });
    expect(response.body.key).toBe('foo');
    expect(response.body.value).toBe('new');
    expect(response.body.version).toBe(2);
    return expect(response.status).toBe(200);
  });

  it('Should return a 200 to a request with new "key" and "value" parameters.', async () => {
    const response = await supertest(app).put('/').send({ key: 'new', value: 'new' });
    expect(response.body.key).toBe('new');
    expect(response.body.value).toBe('new');
    expect(response.body.version).toBe(1);
    return expect(response.status).toBe(200);
  });
})
